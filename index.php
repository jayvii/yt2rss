<?php
/*
* SPDX-FileCopyrightText: 2024 JayVii <jayvii[AT]posteo[DOT]de>
* SPDX-License-Identifier: AGPL-3.0-or-later
*/

if (file_exists("./env.php")) {
    include "./env.php";
}

function analyze_video($video_file) {
    include_once("3rdparty/getid3/getid3/getid3.php");
    $getID3 = new getID3;
    $video_info = $getID3->analyze($video_file);
    return $video_info;
}

// Authentification
if ($_GET["auth"] != $auth_key && !is_null($auth_key)) {
    $auth = false;
} else {
    $auth = true;
}

if (!is_null($_GET["channel"]) && $auth) {
    
    // Fetch Youtube XML Feed
    $channel_xml = file(
        "https://www.youtube.com/feeds/videos.xml?channel_id=" . basename($_GET["channel"])
    );
    // Replace un-parsable items
    $channel_xml = str_replace(
        array("yt:", "media:"),
        array("yt_", "media_"),
        $channel_xml
    );
    // Cast Array to string
    $channel_xml = implode(PHP_EOL, $channel_xml);
    // Parse XML
    $channel_xml = simplexml_load_string($channel_xml);
    $channel_xml = json_encode($channel_xml);
    $channel_xml = json_decode($channel_xml, true);


    // Construct Podcatcher XML
    $rss_xml = "<rss " .
        "version=\"2.0\" " .
        "xmlns:atom=\"http://www.w3.org/2005/Atom\" " .
        "xmlns:content=\"http://purl.org/rss/1.0/modules/content/\" " .
        "xmlns:itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\" " .
        "xmlns:dc=\"http://purl.org/dc/elements/1.1/\" " .
        "xmlns:podcast=\"https://podcastindex.org/namespace/1.0\"" .
        ">\n<channel>\n";
    $rss_xml = $rss_xml .
        "<docs>http://www.rssboard.org/rss-specification</docs>\n";
    $rss_xml = $rss_xml . "<title>" .
        str_replace(
            array("&"),
            "&amp;",
            $channel_xml["title"]
        ) . "</title>\n";
    $channel_id = str_replace(
        array("yt_channel:"),
        "",
        $channel_xml["id"]
    );
    $rss_xml = $rss_xml . "<link>https://www.youtube.com/channel/" .
        basename($_GET["channel"]) . "</link>\n";
    $rss_xml = $rss_xml . "<description>" . 
        str_replace(
            array("&"),
            "&amp;",
            $channel_xml["title"]
        ) .
        "</description>\n";
    $rss_xml = $rss_xml . "<pubDate>" . $channel_xml["published"] .
        "</pubDate>\n";
    // FIXME: fetch channel image rather than first video image
    $video_id = str_replace(
        array("yt_video:"),
        "",
        $channel_xml["entry"][0]["id"]
    );
    $rss_xml = $rss_xml . "<itunes:image href=\"https://i4.ytimg.com/vi/" .
        $video_id . "/hqdefault.jpg\"/>\n";
    $rss_xml = $rss_xml . "<atom:link href=\"https://" .
        $_SERVER["SERVER_NAME"] .
        "/?channel=" . basename($_GET["channel"]);
    if (!is_null($auth_key)) {
        $rss_xml = $rss_xml . "&amp;auth=" . $auth_key;
    }
    $rss_xml = $rss_xml . "\"" .
        " rel=\"self\" type=\"application/rss+xml\"/>\n";

    // Add media items
    foreach ($channel_xml["entry"] as $entry) {
        // Skip if title matches "exclude"
        if (!is_null($_GET["exclude"])) {
            $excluded = strstr($entry["title"], rawurldecode($_GET["exclude"]));
            if ($excluded) continue;
        }
        // Skip if title does not match "include"
        if (!is_null($_GET["include"])) {
            $included = strstr($entry["title"], rawurldecode($_GET["include"]));
            if (!$included) continue;
        }
        $video_id = str_replace(array("yt_video:"), "", $entry["id"]);
        // Get Video Length, size and type
        if (file_exists($video_id . ".opus")) {
            $video_info = analyze_video($video_id . ".opus");
            $video_size = $video_info["filesize"];
            $video_duration = $video_info["playtime_string"];
            $video_ftype = $video_info["mime_type"];
        } else {
            $video_size = 0;
            $video_duration = "00:00:00";
            $video_ftype = "audio/ogg";
        }
        $rss_xml = $rss_xml . "<item>\n";
        $rss_xml = $rss_xml . "<title>" .
            str_replace(array("&"), "&amp;", $entry["title"]) . "</title>\n";
        // FIXME: fetch true description!
        $rss_xml = $rss_xml . "<description>" .
            "Video-Link:" . PHP_EOL .
            $entry["link"]["@attributes"]["href"] . PHP_EOL . PHP_EOL .
            str_replace(
                array("&"),
                "&amp;",
                $entry["media_group"]["media_description"]
            ) . "</description>\n";
        $rss_xml = $rss_xml . "<itunes:author>" . 
            str_replace(
                array("&"),
                "&amp;",
                $entry["author"]["name"]
            ) .
            "</itunes:author>\n";
        $rss_xml = $rss_xml . "<pubDate>" . $entry["published"] .
            "</pubDate>\n";
        $rss_xml = $rss_xml . "<itunes:image href=\"https://i1.ytimg.com/vi/" .
            $video_id . "/hqdefault.jpg\"/>\n";
        $rss_xml = $rss_xml . "<enclosure url=\"https://" .
            $_SERVER["SERVER_NAME"] .
            "/?video=" . $video_id;
        if (!is_null($auth_key)) {
            $rss_xml = $rss_xml . "&amp;auth=" . $auth_key;
        }
        $rss_xml = $rss_xml . "\"".
            " type=\"" . $video_ftype . "\" length=\"" . $video_size . "\"/>\n";
        $rss_xml = $rss_xml . "<itunes:duration>" . $video_duration .
            "</itunes:duration>\n";
        $rss_xml = $rss_xml . "</item>\n";
    }

    $rss_xml = $rss_xml . "</channel>\n</rss>\n";
    header("Content-type: application/xml");
    print_r($rss_xml);
    
} else if (!is_null($_GET["video"]) && $auth) {
    $download_retry = 0;
    while (
        !file_exists(basename($_GET["video"]) . ".opus") &&
        $download_retry <= 3
    ) {
        $download_retry++;
        passthru(
            "yt-dlp " .
            "-x " .
            "--audio-format opus " .
            "-o '%(id)s.%(ext)s' " .
            "https://www.youtube.com/watch?v=" . basename($_GET["video"])
        );
    }
    // If file has been downloaded properly, check whether the file is valid
    if (!(analyze_video(basename($_GET["video"]) . ".opus")["playtime_seconds"] > 0)) {
        // Remove if unvalid
        unlink(basename($_GET["video"]) . ".opus");
    }
    // If file still exists, return to user
    if (file_exists(basename($_GET["video"]) . ".opus")) {
        header("content-type: audio/ogg; codec=opus");
        header(
            "content-length: " . filesize(basename($_GET["video"]) . ".opus")
        );
        header(
            "content-disposition: inline; filename=" .
            basename($_GET["video"]) . ".opus"
        );
        readfile(basename($_GET['video']) . ".opus");
    } else {
        // otherwise return error and exit
        http_response_code(404);
        die();
    }
} else {
    echo "<html><head><title>yt2rss</title></head><body>";
    echo "<h1>yt2rss</h1>";
    echo "<p><a href=\"https://codeberg.org/jayvii/yt2rss\">Learn more</a></p>";
    
    // Usage Info
    echo "<p>Usage:</p>";
    echo "<ul>";
    echo "<li>https://" . $_SERVER["SERVER_NAME"] .
        "/?channel=UCt_Y3j8CpXx366et1RaHACA</li>";
    echo "<li>https://" . $_SERVER["SERVER_NAME"] . "/?video=TV8tEq56vHI</li>";
    echo "</ul>";
    echo "<p>Videos can be included or excluded based on title-matches:</p>";
    echo "<ul>";
    echo "<li>Include only videos whose title contain \"vlog time\":" . PHP_EOL . "https://" . $_SERVER["SERVER_NAME"] .
        "/?channel=UCt_Y3j8CpXx366et1RaHACA&include=vlog%20time</li>";
    echo "<li>Exclude all videos whose title contain \"vlog time\":" . PHP_EOL . "https://" . $_SERVER["SERVER_NAME"] . "/?channel=UCt_Y3j8CpXx366et1RaHACA&exclude=vlog%20time</li>";
    echo "</ul>";

    // Authentification
    if (!is_null($auth_key)) {
        echo "<p>This Service requires an authentification key. " .
            "If you have one, add it to the request URLs with:</p>";
        echo "<ul>";
        echo "<li>https://" . $_SERVER["SERVER_NAME"] .
            "/?auth=MY_KEY&channel=UCt_Y3j8CpXx366et1RaHACA</li>";
        echo "<li>https://" . $_SERVER["SERVER_NAME"] . "/?auth=MY_KEY&video=TV8tEq56vHI</li>";
        echo "</ul>";
        echo "<p>If you do not have an authentification key, please contact the server admin.</p>";
        if ($auth) {
            echo "<p style='color:white;background-color:green;'>You are authenticated :)</p>";
        } else {
            echo "<p style='color:white;background-color:red;'>You are NOT authenticated :(</p>";
        }
    }
    echo "</body></html>";
}
?>
