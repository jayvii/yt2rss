# yt2rss

Transforms a youtube channel into a podcast RSS feed to insert into a podcatcher.

## Usage:

Currently, there are two endpoints for the yt2rss script/service:

* index.php?channel=UCt_Y3j8CpXx366et1RaHACA
* index.php?video=TV8tEq56vHI

Assuming you host this service under the URL "https://yt2rss.example.com", insert following RSS link into your podcatcher:

```
https://yt2rss.example.com/?channel=UCt_Y3j8CpXx366et1RaHACA
```

Individual video entries can be fetched via:

```
https://yt2rss.example.com/?video=TV8tEq56vHI
```

If the video has not been fetched before, this will take a few seconds, as the server needs to download and convert the video to an `.opus` audio file before it can serve it.

Videos within a feed can be included or excluded based on title-matches:

Include only videos whose title contain "vlog time":

```
https://yt2rss.example.com/?channel=UCt_Y3j8CpXx366et1RaHACA&include=vlog%20time
```

Exclude all videos whose title contain "vlog time":

```
https://yt2rss.example.com/?channel=UCt_Y3j8CpXx366et1RaHACA&exclude=vlog%20time
```

## Additional configuration

It is advised to only allow trusted parties to use your service. yt2rss uses a simple authentication key for this, which is included in the URL:

```
https://yt2rss.example.com/?auth=MY_KEY&channel=UCt_Y3j8CpXx366et1RaHACA
https://yt2rss.example.com/?auth=MY_KEY&video=TV8tEq56vHI
```

You can set an auth-key in the `env.php` file. If the value is set to something other than `null` (the default), authentication is enabled.

## Requirements:

- [getID3](https://github.com/JamesHeinrich/getID3)
- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- PHP-7.2 or higher

## Further notes

### Episode durations are not shown correctly on first load

Duration times of entries are unknown until they have been downloaded. The podcast feed (`?channel=...`) will show episode durations as "00:00:00" unless the according episode is already present in the webroot (has been accessed by the podcatcher or any other client, like a webbrowser).

### The webroot grows bigger / old episodes are never removed from the server

The script will never "cleanup" audio files from the webdir root directory, after they have been downloaded by the client. Depending on your usecase scenario, it might be a good idea to setup a cronjob for that. The example below will look every night at 00:00 for `.opus` files in the web-directory (in this example `/var/www/yt2rss`, adjust accordingly) that have been created (downloaded) over 7 days ago.

```cron
# |----------- minutes
# |
# | |--------- hours
# | |
# | | |------- day of month
# | | |
# | | | |----- month
# | | | |
# | | | | |--- day of week
# | | | | |
# | | | | | |- command 
# | | | | | | 
  0 0 * * * find /var/www/yt2rss/ -type f -name "*.opus" ! -newermt $(date +%Y-%m-%d --date "-7 days") -exec rm {} \;
```
