<?php
/*
* SPDX-FileCopyrightText: 2024 JayVii <jayvii[AT]posteo[DOT]de>
* SPDX-License-Identifier: AGPL-3.0-or-later
*/

/* Authentification Key.
* If set, videos and feeds can only be loaded if auth key matches.
* Set to null if it should not be used.
*/
//$auth_key="f7331f90cb24e34c8cfd59e3ad46bbe1";
$auth_key=null;

?>
